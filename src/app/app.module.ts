import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import {MapPage} from '../pages/map/map'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Facebook } from '@ionic-native/facebook';
import { PetDetailPage } from '../pages/pet-detail/pet-detail';
import { AddPetPage} from '../pages/add-pet/add-pet';

// providers
import { Singleton } from '../providers/singleton/singleton';
import { HomeModel } from '../providers/home-model/home-model';
import {Camera} from '@ionic-native/camera';
import { GoogleMaps} from '@ionic-native/google-maps';
import {Geolocation} from '@ionic-native/geolocation';
import { PinProvider } from '../providers/pin/pin';
import { HTTP } from '@ionic-native/http';
import { HttpModule } from '@angular/http';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    PetDetailPage,
    AddPetPage,
    MapPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    PetDetailPage,
    AddPetPage,
    MapPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Singleton,
    HomeModel,
    Camera,
    GoogleMaps,
    Geolocation,
    PinProvider,
    HTTP,
    SocialSharing
  ]
})
export class AppModule {}
