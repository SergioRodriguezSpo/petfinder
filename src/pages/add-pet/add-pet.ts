import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { HomeModel } from '../../providers/home-model/home-model';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HTTP } from '@ionic-native/http';
import {Geolocation} from '@ionic-native/geolocation';
/**
 * Generated class for the AddPetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-pet',
  templateUrl: 'add-pet.html',
})
export class AddPetPage {
  imagePath: string = "assets/imgs/animal_placeholder.png"// default placeholder
  name:string;
  description:string;
  animal:string;
  race:string;
  sex:string;
  age:number;
  telefone:string;

  lat: number = -19.9440882;//default fumec
  lng: number = -43.9285403; //default fumec



  constructor(private geolocation: Geolocation,public navCtrl: NavController, public navParams: NavParams, public view: ViewController, public camera: Camera,private http: HTTP) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPetPage');
  }

  openCamera(){
    console.log("click");
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imagePath = 'data:image/jpeg;base64,' + imageData;
     }, (err) => {
      // Handle error
     });
  }
  close(){
    this.view.dismiss();
  }

  saveData(){
      //todo validate fields
    this.getUserLocation(); // get user current location

    //REAL API
    // this.http.post('http://localhost:3000/pet', 
    // { 
    //   name : this.name,
    //   description: this.description,
    //   imagePath: this.imagePath,
    //   animalType: this.animal,
    //   sex: this.sex,
    //   age: this.age,
    //   cellphone: this.telefone
    // }, 
    // {
    //  headers: { 'Content-Type': 'application/json' }
    // }).then(data => {
    //   console.log(data.data); // json do servidor
    //   var homeModel = new HomeModel()
    //   homeModel = JSON.parse(data.data);
    //   this.view.dismiss(homeModel);
    // }).catch(error => {
    //   alert(error.data);
    // });

  

    //fake api object
    var homeModel = new HomeModel()
    homeModel.id = 100; 
    homeModel.name = this.name;
    homeModel.description =this.description;
    homeModel.imagePath = this.imagePath;
    homeModel.animalType = this.animal;
    homeModel.sex = this.sex;
    homeModel.age = this.age;
    homeModel.cellphone = this.telefone;
    homeModel.lat = this.lat;
    homeModel.lng = this.lng;
    this.view.dismiss(homeModel);
  }

  getUserLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude
      this.lng = resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
}

