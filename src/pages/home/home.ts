import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { HomeModel } from '../../providers/home-model/home-model';
import { PetDetailPage} from '../pet-detail/pet-detail'
import { AddPetPage} from '../add-pet/add-pet'
import { HTTP } from '@ionic-native/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  homeList:Array<HomeModel>
  
  //NÃO estou usando minha api real aqui pois você teria q configurar o ambiente..
  //Deixei tudo mockado para facilitar o teste do codigo.
  // deixei comentado as calls da api

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,private http: HTTP) {
  }

  ionViewDidLoad() {
    this.homeList = new Array<HomeModel>();


    this.loadApi()
  }

  loadApi(){
    //should call real api
    var fakeApiResponseString = "[{\"id\": 1,\"name\": \"Josefilda\",\"description\": \"ela é muito fofa\",\"imagePath\": \"https://t2.ea.ltmcdn.com/pt/images/9/1/5/img_meu_cachorro_tem_o_nariz_frio_e_normal_21519_600.jpg\",\"animalType\": \"Cachorro\",\"sex\": \"M\",\"age\": 2,\"cellphone\": \"(51)99984-4232\",\"lat\": -19.9435552,\"lng\": -43.9289475},{\"id\": 2,\"name\": \"Abel\",\"description\": \"melhor gato do mundo!\",\"imagePath\": \"https://meusanimais.com.br/wp-content/uploads/2015/03/4-gato-enfermo.jpg\",\"animalType\": \"Gato\",\"sex\": \"M\",\"age\": 5,\"cellphone\": \"(51)994324-4232\",\"lat\": -19.9443217,\"lng\": -43.9267159},{\"id\": 3,\"name\": \"Calopsita do mal\",\"description\": \"encontrei ela em uma moita!\",\"imagePath\": \"https://blog.petnanet.com.br/wp-content/uploads/2017/09/106262-voce-sabe-como-cuidar-de-uma-calopsita-aprenda-aqui-700x465.jpg\",\"animalType\": \"Calopsita\",\"sex\": \"F\",\"age\": 1,\"cellphone\": \"(51)22984-4232\",\"lat\": -19.9415784,\"lng\": -43.9253855}]";
    this.homeList = JSON.parse(fakeApiResponseString);

    // REAL API CALL EXAMPLE

    // this.http.get('http://localhost:3000/home', {}, {}).then(data => {
    //   console.log(data.data); // json do servidor
    //   this.homeList = JSON.parse(data.data);
    // })
    // .catch(error => {
    //   alert(error.data)
    // });
  }

  onCardTapped(model:HomeModel){
    this.navCtrl.push(PetDetailPage, { pet: model})
  }

  addPet(){
    let addPlanModal = this.modalCtrl.create(AddPetPage);
    addPlanModal.onDidDismiss((pet) => {
      if(pet){
        this.savePet(pet);
      }
    });
    
    addPlanModal.present();
  }

  savePet(pet:HomeModel){
    this.homeList.push(pet);
  }
}
