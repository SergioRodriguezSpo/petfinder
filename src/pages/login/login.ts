import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Facebook} from '@ionic-native/facebook';
import {Singleton } from '../../providers/singleton/singleton';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  userInfo: any = {};
  fbAccessToken: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,private fb: Facebook) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  loginWithFB(){
    this.fb.login(["public_profile","email"]).then( loginRes => {
      this.fb.api('me/?fields=id,email,first_name,picture',["public_profile","email"]).then( apiRes => {
        this.userInfo = apiRes;
        this.fbAccessToken = loginRes.authResponse.accessToken;
        this.callApi();
    
      }).catch( apiErr => console.log(apiErr));
    }).catch( loginErr => console.log(loginErr) )
  }

  callApi(){
    //TODO fake api, sava data
    this.navCtrl.setRoot(TabsPage);
  }
}
