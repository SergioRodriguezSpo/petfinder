import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';
import { Component } from "@angular/core/";
import {Geolocation} from '@ionic-native/geolocation';
import {PinProvider} from '../../providers/pin/pin'

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  map: GoogleMap;
  pinList: Array<PinProvider>

  constructor(private geolocation: Geolocation) { }

  ionViewDidLoad() {
    //TODO load fake api
    var fakeApiResponse = "[ { \"id\": 1, \"title\": \"Josefilda\", \"lat\": -19.9435552, \"lng\": -43.9289475 }, { \"id\": 2, \"title\": \"Abel\", \"lat\": -19.9443217, \"lng\": -43.9267159 }, { \"id\": 3, \"title\": \"Calopsita do mal\", \"lat\": -19.9415784, \"lng\": -43.9253855 } ]";
    this.pinList = JSON.parse(fakeApiResponse);
   
    this.getLocation();
  }

  getLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude

      this.loadMap(resp.coords.latitude,resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
       this.loadMap(-19.9440882,-43.9285403);
     });
  }
  loadMap(lat: any, lng:any) {
    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: lat,
           lng: lng
         },
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.setMapPin();
  }

  setMapPin(){
      this.pinList.forEach(element => {
      let marker: Marker = this.map.addMarkerSync({
        title: element.title,
        icon: 'blue',
        animation: 'DROP',
        zIndex: element.id,
        position: {

          lat: element.lat,
          lng: element.lng
        }
      });

      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(e => {
        //todo send to details
          //alert(this.pinList[e.zIndex].id);
      });
    });
  }
}

