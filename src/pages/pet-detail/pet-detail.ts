import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomeModel } from '../../providers/home-model/home-model';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';

import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the PetDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pet-detail',
  templateUrl: 'pet-detail.html',
})
export class PetDetailPage {
  petModel:HomeModel = this.navParams.get('pet');
  map: GoogleMap;

  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing:SocialSharing) {
  }

  ionViewDidLoad() {
    this.loadMap(this.petModel.lat,this.petModel.lng)

    
   
  }

  share(){
    this.socialSharing.share(this.petModel.name + " " + "http://www.caoviver.com.br" ).then(()=>{

    }).catch(()=>{

    })
  }
  loadMap(lat: any, lng:any) {
    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: lat,
           lng: lng
         },
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.map.setAllGesturesEnabled(false);
    this.setMapPin();
  }

  setMapPin(){
      let marker: Marker = this.map.addMarkerSync({
        title: this.petModel.name,
        icon: 'blue',
        animation: 'DROP',
        position: {

          lat: this.petModel.lat,
          lng: this.petModel.lng
        }
    });
  }

}
