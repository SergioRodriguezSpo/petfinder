import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HomeModelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomeModel {
  id: number;
  name: string;
  description: string;
  imagePath: string;
  animalType: string;
  sex: string;
  age: number;
  cellphone: string;
  lat: number;
  lng: number;
}
